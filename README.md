# Slime Castle 2
version 1.0

## Download link:

https://gitlab.com/StephanieCheng/slime-castle-unity/-/archive/master/slime-castle-unity-master.zip

## Installation
1. Download .zip archive
2. Extract
3. Run SlimeCastle2.exe  

  
## Controls
A D - forward backward
S W - change lanes
E - interact with switches
Space - jump